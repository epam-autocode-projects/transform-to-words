using System;
using System.Text;

#pragma warning disable CA1822

namespace TransformToWordsTask
{
    /// <summary>
    /// Provides transformer methods.
    /// </summary>
    public sealed class Transformer
    {
        /// <summary>
        /// Converts number's digital representation into words.
        /// </summary>
        /// <param name="number">Number to convert.</param>
        /// <returns>Words representation.</returns>
        public string TransformToWords(double number)
        {
            string s = number.ToString(System.Globalization.CultureInfo.CurrentCulture);
            StringBuilder res = new StringBuilder();

            for (int i = 0; i < s.Length; i++)
            {
                if (number == double.Epsilon)
                {
                    res.Append(" Double Epsilon");
                    break;
                }

                if (double.IsNaN(number))
                {
                    res.Append(" NaN");
                    break;
                }

                if (number == double.NegativeInfinity)
                {
                    res.Append(" Negative Infinity");
                    break;
                }

                if (number == double.PositiveInfinity)
                {
                    res.Append(" Positive Infinity");
                    break;
                }

                switch (s[i])
                {
                    case 'E':
                        {
                            res.Append(" E plus");
                            break;
                        }

                    case 'e':
                        {
                            res.Append(" E plus");
                            break;
                        }

                    case '-':
                        {
                            res.Append(" minus");
                            break;
                        }

                    case '.':
                        {
                            res.Append(" point");
                            break;
                        }

                    case '0':
                        {
                            res.Append(" zero");
                            break;
                        }

                    case '1':
                        {
                            res.Append(" one");
                            break;
                        }

                    case '2':
                        {
                            res.Append(" two");
                            break;
                        }

                    case '3':
                        {
                            res.Append(" three");
                            break;
                        }

                    case '4':
                        {
                            res.Append(" four");
                            break;
                        }

                    case '5':
                        {
                            res.Append(" five");
                            break;
                        }

                    case '6':
                        {
                            res.Append(" six");
                            break;
                        }

                    case '7':
                        {
                            res.Append(" seven");
                            break;
                        }

                    case '8':
                        {
                            res.Append(" eight");
                            break;
                        }

                    case '9':
                        {
                            res.Append(" nine");
                            break;
                        }
                }

                if (Math.Abs(number) == 0.0d && res.ToString() != " zero")
                {
                    res.Append(" zero");
                    break;
                }
                else if (Math.Abs(number) == 0.0d && res.ToString() == " zero")
                {
                    break;
                }
            }

            string s1 = char.ToUpper(res[1], System.Globalization.CultureInfo.CurrentCulture) + res.ToString().Substring(2);
            return s1;
        }
    }
}